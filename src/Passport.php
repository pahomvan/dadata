<?php
/**
 * Created at: 07.04.2018 12:42
 * @author commercito <commercito@gmail.com>
 * @link http://commercito.ru/
 * @copyright Copyright (c) 2018 commercito
 */

namespace commercito\dadata\src;

/**
 * @method string getNumber()
 * @method string getQc()
 * @method string getSeries()
 * @method string getSource()
 */
class Passport extends AbstractParent
{
    /**
     * Get important values from dadata service response
     * @return array
     */
    public function getImportantValues()
    {
        return ['Nothing interesting here'];
    }
}