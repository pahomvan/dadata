<?php
/**
 * Created at: 07.04.2018 12:41
 * @author commercito <commercito@gmail.com>
 * @link http://commercito.ru/
 * @copyright Copyright (c) 2018 commercito
 */

namespace commercito\dadata\src;

/**
 * @method string getGender()
 * @method string getName()
 * @method string getPatronymic()
 * @method string getQc()
 * @method string getResult()
 * @method string getResultAblative()
 * @method string getResultDative()
 * @method string getResultGenitive()
 * @method string getSource()
 * @method string getSurname()
 */
class Name extends AbstractParent
{
    /**
     * Get important values from dadata service response
     * @return array
     */
    public function getImportantValues()
    {
        $importantKeys = [
            'result','result_genitive','result_dative','result_ablative','surname','name','patronymic','gender'
        ];
        $result = array_intersect_key($this->response,$this->addValue($importantKeys));
        $result['kto'] = $this->response['result'];
        $result['kogo'] = $this->response['result_genitive'];
        $result['komu'] = $this->response['result_dative'];
        $result['kem'] = $this->response['result_ablative'];
        return $result;
    }
}