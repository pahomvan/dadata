<?php
/**
 * Created at: 07.04.2018 12:43
 * @author commercito <commercito@gmail.com>
 * @link http://commercito.ru/
 * @copyright Copyright (c) 2018 commercito
 */

namespace commercito\dadata\src;

/**
 * @method string getBrand()
 * @method string getModel()
 * @method string getQc()
 * @method string getResult()
 * @method string getSource()
 */
class Vehicle extends AbstractParent
{
    /**
     * Get important values from dadata service response
     * @return array
     */
    public function getImportantValues()
    {
        return ['Nothing interesting here'];
    }
}