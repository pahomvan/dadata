<?php
/**
 * Created at: 07.04.2018 12:41
 * @author commercito <commercito@gmail.com>
 * @link http://commercito.ru/
 * @copyright Copyright (c) 2018 commercito
 */

namespace commercito\dadata\src;

/**
 * @method string getEmail()
 * @method string getQc()
 * @method string getSource()
 */
class Email extends AbstractParent
{
    /**
     * Get important values from dadata service response
     * @return array
     */
    public function getImportantValues()
    {
        $importantKeys = [
            'email'
        ];
        $result = array_intersect_key($this->response,$this->addValue($importantKeys));
        return $result;
    }
}