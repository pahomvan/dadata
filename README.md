# dadata
work with [DaData service](https://dadata.ru/about/)

Standardization API / [API стандартизации](https://dadata.ru/api/clean/)

Suggestions API / [API подсказок](https://dadata.ru/api/suggest/)

[![Latest Stable Version](https://poser.pugx.org/commercito/dadata/version)](https://packagist.org/packages/commercito/dadata)
[![Total Downloads](https://poser.pugx.org/commercito/dadata/downloads)](https://packagist.org/packages/commercito/dadata)
[![License](https://poser.pugx.org/commercito/dadata/license)](https://packagist.org/packages/commercito/dadata)

* address
* bank
* birthdate 
* email 
* name
* party
* passport 
* phone
* vehicle

***
read the [WIKI](https://gitlab.com/commercito/dadata/wikis/Главная)
***
https://packagist.org/packages/commercito/dadata