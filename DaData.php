<?php
/**
 * Created at: 07.04.2018 10:59
 * @author commercito <commercito@gmail.com>
 * @link http://commercito.ru/
 * @copyright Copyright (c) 2018 commercito
 */

namespace commercito\dadata;

use commercito\dadata\src\Address;
use commercito\dadata\src\Birthdate;
use commercito\dadata\src\Email;
use commercito\dadata\src\Name;
use commercito\dadata\src\Passport;
use commercito\dadata\src\Phone;
use commercito\dadata\src\Vehicle;

use commercito\dadata\src\Bank;
use commercito\dadata\src\Party;

/**
 * Main class to work with dadata.ru service
 *
 * @package commercito\dadata
 */
class DaData
{
    protected $config = [];
    protected $suggestIs = false;

    /**
     * DaData constructor.
     * @param array $config
     * @param bool $suggest
     */
    public function __construct(array $config, $suggest = false)
    {
        $this->config = $config;
        $this->suggestIs = $suggest;
    }

    /**
     * @param array $data
     * @return Address
     */
    public function address(array $data)
    {
        $object = new Address($this->config, $this->suggestIs);
        return $object->request($data);
    }

    /**
     * @param array $data
     * @return Bank
     */
    public function bank(array $data)
    {
        $object = new Bank($this->config, $this->suggestIs);
        return $object->request($data);
    }

    /**
     * @param array $data
     * @return Birthdate
     */
    public function birthdate(array $data)
    {
        $object = new Birthdate($this->config, $this->suggestIs);
        return $object->request($data);
    }

    /**
     * @param array $data
     * @return Email
     */
    public function email(array $data)
    {
        $object = new Email($this->config, $this->suggestIs);
        return $object->request($data);
    }

    /**
     * @param array $data
     * @return Name
     */
    public function name(array $data)
    {
        $object = new Name($this->config, $this->suggestIs);
        return $object->request($data);
    }

    /**
     * @param array $data
     * @return Party
     */
    public function party(array $data)
    {
        $object = new Party($this->config, $this->suggestIs);
        return $object->request($data);
    }

    /**
     * @param array $data
     * @return Passport
     */
    public function passport(array $data)
    {
        $object = new Passport($this->config, $this->suggestIs);
        return $object->request($data);
    }

    /**
     * @param array $data
     * @return Phone
     */
    public function phone(array $data)
    {
        $object = new Phone($this->config, $this->suggestIs);
        return $object->request($data);
    }

    /**
     * @param array $data
     * @return Vehicle
     */
    public function vehicle(array $data)
    {
        $object = new Vehicle($this->config, $this->suggestIs);
        return $object->request($data);
    }

    /**
     * Array pretty print
     * @param $arr
     */
    public function printR($arr)
    {
        echo "\n\n<pre>\n";
        print_r($arr);
        echo "\n</pre>\n\n";
    }

}