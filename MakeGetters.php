<?php
/**
 * Created at: 07.04.2018 10:01
 * @author commercito <commercito@gmail.com>
 * @link http://commercito.ru/
 * @copyright Copyright (c) 2018 commercito
 */

namespace commercito\dadata;

/**
 * Class for generating PhpDoc getters
 * based on the keys from the answer
 * to the dadata.ru service
 *
 * @package commercito\dadata
 */
class MakeGetters
{

    public static $exampleParty = <<<JSON
{
    "suggestions": [
        {
            "value": "ПАО СБЕРБАНК",
            "unrestricted_value": "ПАО СБЕРБАНК",
            "data": {
                "kpp": "773601001",
                "capital": null,
                "management": {
                    "name": "Греф Герман Оскарович",
                    "post": "Президент, председатель правления"
                },
                "branch_type": "MAIN",
                "branch_count": 93,
                "source": null,
                "qc": null,
                "hid": "b347aa7ff5df1158063c052ca6c8c283c942d03a4d8b2ca5168899b26a9c246d",
                "type": "LEGAL",
                "state": {
                    "status": "ACTIVE",
                    "actuality_date": 1506988800000,
                    "registration_date": 677376000000,
                    "liquidation_date": null
                },
                "opf": {
                    "type": null,
                    "code": "12247",
                    "full": "Публичное акционерное общество",
                    "short": "ПАО"
                },
                "name": {
                    "full_with_opf": "ПУБЛИЧНОЕ АКЦИОНЕРНОЕ ОБЩЕСТВО \"СБЕРБАНК РОССИИ\"",
                    "short_with_opf": "ПАО СБЕРБАНК",
                    "latin": null,
                    "full": "СБЕРБАНК РОССИИ",
                    "short": "СБЕРБАНК"
                },
                "inn": "7707083893",
                "ogrn": "1027700132195",
                "okpo": null,
                "okved": "64.19",
                "okveds": null,
                "authorities": null,
                "documents": null,
                "licenses": null,
                "address": {
                    "value": "г Москва, ул Вавилова, д 19",
                    "unrestricted_value": "г Москва, Академический р-н, ул Вавилова, д 19",
                    "data": {}
                },
                "phones": null,
                "emails": null,
                "ogrn_date": 1029456000000,
                "okved_type": "2014"
            }
        }
    ]
}
JSON;


    public static $exampleBank = <<<JSON
{
    "suggestions": [
        {
            "value": "ПАО СБЕРБАНК",
            "unrestricted_value": "ПАО СБЕРБАНК",
            "data": {
                "opf": {
                    "type": "BANK",
                    "full": "Сбербанк России",
                    "short": "СБ"
                },
                "name": {
                    "payment": "ПАО СБЕРБАНК",
                    "full": "ПУБЛИЧНОЕ АКЦИОНЕРНОЕ ОБЩЕСТВО \"СБЕРБАНК РОССИИ\"",
                    "short": "ПАО СБЕРБАНК"
                },
                "bic": "044525225",
                "swift": "SABRRUMMXXX",
                "okpo": "00032537",
                "correspondent_account": "30101810400000000225",
                "registration_number": "1481",
                "rkc": {
                    "opf": {
                        "type": "RKC",
                        "full": "Главное управление/Отделение (НБ)",
                        "short": "ГУ/О"
                    },
                    "name": {
                        "payment": "ГУ БАНКА РОССИИ ПО ЦФО",
                        "full": null,
                        "short": null
                    },
                    "bic": "044525000",
                    "swift": null,
                    "okpo": "09201220",
                    "correspondent_account": null,
                    "registration_number": null,
                    "rkc": null,
                    "address": {
                        "value": "115035, МОСКВА 35, УЛ.БАЛЧУГ,2",
                        "unrestricted_value": "115035, МОСКВА 35, УЛ.БАЛЧУГ,2",
                        "data": null
                    },
                    "phone": null,
                    "state": {
                        "status": "ACTIVE",
                        "actuality_date": 1455580800000,
                        "registration_date": 889488000000,
                        "liquidation_date": null
                    }
                },
                "address": {
                    "value": "г Москва, ул Вавилова, д 19",
                    "unrestricted_value": "г Москва, ул Вавилова, д 19",
                    "data": {}
                },
                "phone": null,
                "state": {
                    "status": "ACTIVE",
                    "actuality_date": 1455580800000,
                    "registration_date": 844387200000,
                    "liquidation_date": null
                }
            }
        }
    ]
}
JSON;


    /**
     * Пример ответа для адреса
     * @var JSONstring
     */
    public static $exampleAddress = <<<JSON
[
    {
    "source": "мск сухонска 11/-89",
    "result": "г Москва, ул Сухонская, д 11, кв 89",
    "postal_code": "127642",
    "country": "Россия",
    "region_fias_id": "0c5b2444-70a0-4932-980c-b4dc0d3f02b5",
    "region_kladr_id": "7700000000000",
    "region_with_type": "г Москва",
    "region_type": "г",
    "region_type_full": "город",
    "region": "Москва",
    "area_fias_id": null,
    "area_kladr_id": null,
    "area_with_type": null,
    "area_type": null,
    "area_type_full": null,
    "area": null,
    "city_fias_id": null,
    "city_kladr_id": null,
    "city_with_type": null,
    "city_type": null,
    "city_type_full": null,
    "city": null,
    "city_area": "Северо-восточный",
    "city_district_fias_id": null,
    "city_district_kladr_id": null,
    "city_district_with_type": "р-н Северное Медведково",
    "city_district_type": "р-н",
    "city_district_type_full": "район",
    "city_district": "Северное Медведково",
    "settlement_fias_id": null,
    "settlement_kladr_id": null,
    "settlement_with_type": null,
    "settlement_type": null,
    "settlement_type_full": null,
    "settlement": null,
    "street_fias_id": "95dbf7fb-0dd4-4a04-8100-4f6c847564b5",
    "street_kladr_id": "77000000000283600",
    "street_with_type": "ул Сухонская",
    "street_type": "ул",
    "street_type_full": "улица",
    "street": "Сухонская",
    "house_fias_id": "5ee84ac0-eb9a-4b42-b814-2f5f7c27c255",
    "house_kladr_id": "7700000000028360004",
    "house_type": "д",
    "house_type_full": "дом",
    "house": "11",
    "block_type": null,
    "block_type_full": null,
    "block": null,
    "flat_type": "кв",
    "flat_type_full": "квартира",
    "flat": "89",
    "flat_area": "34.6",
    "square_meter_price": "198113",
    "flat_price": "6854710",
    "postal_box": null,
    "fias_id": "5ee84ac0-eb9a-4b42-b814-2f5f7c27c255",
    "fias_code": "77000000000000028360004",
    "fias_level": "8",
    "fias_actuality_state": "0",
    "kladr_id": "7700000000028360004",
    "capital_marker": "0",
    "okato": "45280583000",
    "oktmo": "45362000",
    "tax_office": "7715",
    "tax_office_legal": "7715",
    "timezone": "UTC+3",
    "geo_lat": "55.8783675",
    "geo_lon": "37.6537388",
    "beltway_hit": "IN_MKAD",
    "beltway_distance": null,
    "qc_geo": 0,
    "qc_complete": 0,
    "qc_house": 2,
    "qc": 0,
    "unparsed_parts": null,
    "metro": [
        {
            "distance": 1.1,
            "line": "Калужско-Рижская",
            "name": "Бабушкинская"
        },
        {
            "distance": 1.2,
            "line": "Калужско-Рижская",
            "name": "Медведково"
        },
        {
            "distance": 2.5,
            "line": "Калужско-Рижская",
            "name": "Свиблово"
        }
    ]
}
]
JSON;

    /**
     * Пример ответа для дня рождения
     * @var JSONstring
     */
    public static $exampleBirthday = <<<JSON
[{
	"source": "24/3/12",
	"birthdate": "24.03.2012",
	"qc": 1
}]
JSON;

    /**
     * Пример ответа для расширенного запроса
     * @var JSONstring
     */
    public static $exampleClean = <<<JSON
{
	"structure": [
		"AS_IS",
		"NAME",
		"ADDRESS",
		"PHONE"
	],
	"data": [
		[{
			"source": "1"
		}, {
			"source": "Федотов Алексей",
			"result": "Федотов Алексей",
			"qc": 0
		}, {
			"source": "Москва, Сухонская улица, 11 кв 89",
			"result": "г Москва, ул Сухонская, д 11, кв 89",
			"qc": 0,
			"unparsed_parts": null
		}, {
			"source": "8 916 823 3454",
			"type": "Мобильный",
			"phone": "+7 916 823-34-54",
			"qc": 0
		}],
		[{
			"source": "2"
		}, {
			"source": "Иванов Сергей Владимирович",
			"result": "Иванов Сергей Владимирович",
			"qc": 0
		}, {
			"source": "мск,улица свободы,65,12",
			"result": "г Москва, ул Свободы, д 65, кв 12",
			"qc": 0,
			"unparsed_parts": null
		}, {
			"source": "495 663-12-53",
			"type": "Стационарный",
			"phone": "+7 495 663-12-53",
			"qc": 0
		}],
		[{
			"source": "3"
		}, {
			"source": "Ольга Павловна Ященко",
			"result": "Ященко Ольга Павловна",
			"qc": 0
		}, {
			"source": "Спб, ул Петрозаводская 8",
			"result": "Россия, г Санкт-Петербург, ул Петрозаводская, д 8",
			"qc": 0,
			"unparsed_parts": null
		}, {
			"source": "457 07 25",
			"type": "Стационарный",
			"phone": "+7 812 457-07-25",
			"qc": 1
		}]
	]
}
JSON;

    /**
     * Пример ответа для почты
     * @var JSONstring
     */
    public static $exampleEmail = <<<JSON
[{
	"source": "serega@yandex/ru",
	"email": "serega@yandex.ru",
	"qc": 4
}]
JSON;

    /**
     * Пример ответа для ФИО
     * @var JSONstring
     */
    public static $exampleName = <<<JSON
[{
	"source": "Срегей владимерович иванов",
	"result": "Иванов Сергей Владимирович",
	"result_genitive": "Иванова Сергея Владимировича",
	"result_dative": "Иванову Сергею Владимировичу",
	"result_ablative": "Ивановым Сергеем Владимировичем",
	"surname": "Иванов",
	"name": "Сергей",
	"patronymic": "Владимирович",
	"gender": "М",
	"qc": 1
}]
JSON;

    /**
     * Пример ответа для паспорта
     * @var JSONstring
     */
    public static $examplePassport = <<<JSON
[{
	"source": "4509 235857",
	"series": "45 09",
	"number": "235857",
	"qc": 0
}]
JSON;

    /**
     * Пример ответа для телефона
     * @var JSONstring
     */
    public static $examplePhone = <<<JSON
[{
	"source": "тел 7165219 доб139",
	"type": "Стационарный",
	"phone": "+7 495 716-52-19 доб. 139",
	"country_code": "7",
	"city_code": "495",
	"number": "7165219",
	"extension": "139",
	"provider": "ОАО \"МГТС\"",
	"region": "Москва",
	"timezone": "UTC+3",
	"qc_conflict": 0,
	"qc": 1
}]
JSON;

    /**
     * Пример ответа для машины
     * @var JSONstring
     */
    public static $exampleVehicle = <<<JSON
[{
	"source": "форд фокус",
	"result": "FORD FOCUS",
	"brand": "FORD",
	"model": "FOCUS",
	"qc": 0
}]
JSON;

    /**
     * Названия методов запроса к API dadata.ru
     * @var array
     */
    private static $apiMethods = [
        'address',
        'birthday',
        'clean',
        'email',
        'name',
        'passport',
        'phone',
        'vehicle',
        'bank',
        'party',
    ];

    /**
     * Формирование ссылок для генерации геттеров
     * для использования в array_map
     * @param $item
     * @return string
     */
    private static function makeLinkToArr($item)
    {
        $href = '?method='.$item;
        return "<a href='{$href}'>{$item}</a>";
    }

    /**
     * Генерациия get-методов
     * на основе ключей из ответов сервиса DaData.ru
     * в формате PhpDoc
     * @param bool $method
     */
    public static function generator($method=false)
    {
        if (!empty($_GET['method'])) {
            $method = $_GET['method'];
        }
        if ($method) {
            $example = 'example'.ucfirst($method);
            $arr = json_decode(self::$$example,true);
            $arrKeys = in_array($method,['bank','party']) ? $arr['suggestions'][0]['data'] : $arr[0];
            $keys = array_keys($arrKeys);
            sort($keys);
            echo "/**\n<br>";
            foreach ($keys as $item) {
                $name = str_replace('_',' ',$item);
                $nameClear = ucwords($name);
                $method = ' * @method string get'.str_replace(' ','',$nameClear).'()';
                echo $method."\n<br>\n";
            }
            echo " */\n";
            echo "<p><a href='{$_SERVER["PHP_SELF"]}'>вернуться назад</a></p>";
        } else {
            $linksArray = array_map('self::makeLinkToArr',self::$apiMethods);
            echo "<h2>Генератор геттеров</h2>
<h3>на основе ключей ответов API dadata.ru</h3>
<p>Просто перейдите по ссылке</p>";
            echo "<pre>";
            print_r($linksArray);
            echo "</pre>";
        }
    }

}