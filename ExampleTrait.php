<?php
/**
 * Created at: 07.04.2018 13:21
 * @author commercito <commercito@gmail.com>
 * @link http://commercito.ru/
 * @copyright Copyright (c) 2018 commercito
 */

namespace commercito\dadata;

/**
 * Trait with response examples from dadata.ru service
 *
 * @package commercito\dadata
 */
trait ExampleTrait
{
    /**
     * Пример ответа для адреса
     * @var JSONstring
     */
    public static $exampleAddress = <<<JSON
[
    {
    "source": "мск сухонска 11/-89",
    "result": "г Москва, ул Сухонская, д 11, кв 89",
    "postal_code": "127642",
    "country": "Россия",
    "region_fias_id": "0c5b2444-70a0-4932-980c-b4dc0d3f02b5",
    "region_kladr_id": "7700000000000",
    "region_with_type": "г Москва",
    "region_type": "г",
    "region_type_full": "город",
    "region": "Москва",
    "area_fias_id": null,
    "area_kladr_id": null,
    "area_with_type": null,
    "area_type": null,
    "area_type_full": null,
    "area": null,
    "city_fias_id": null,
    "city_kladr_id": null,
    "city_with_type": null,
    "city_type": null,
    "city_type_full": null,
    "city": null,
    "city_area": "Северо-восточный",
    "city_district_fias_id": null,
    "city_district_kladr_id": null,
    "city_district_with_type": "р-н Северное Медведково",
    "city_district_type": "р-н",
    "city_district_type_full": "район",
    "city_district": "Северное Медведково",
    "settlement_fias_id": null,
    "settlement_kladr_id": null,
    "settlement_with_type": null,
    "settlement_type": null,
    "settlement_type_full": null,
    "settlement": null,
    "street_fias_id": "95dbf7fb-0dd4-4a04-8100-4f6c847564b5",
    "street_kladr_id": "77000000000283600",
    "street_with_type": "ул Сухонская",
    "street_type": "ул",
    "street_type_full": "улица",
    "street": "Сухонская",
    "house_fias_id": "5ee84ac0-eb9a-4b42-b814-2f5f7c27c255",
    "house_kladr_id": "7700000000028360004",
    "house_type": "д",
    "house_type_full": "дом",
    "house": "11",
    "block_type": null,
    "block_type_full": null,
    "block": null,
    "flat_type": "кв",
    "flat_type_full": "квартира",
    "flat": "89",
    "flat_area": "34.6",
    "square_meter_price": "198113",
    "flat_price": "6854710",
    "postal_box": null,
    "fias_id": "5ee84ac0-eb9a-4b42-b814-2f5f7c27c255",
    "fias_code": "77000000000000028360004",
    "fias_level": "8",
    "fias_actuality_state": "0",
    "kladr_id": "7700000000028360004",
    "capital_marker": "0",
    "okato": "45280583000",
    "oktmo": "45362000",
    "tax_office": "7715",
    "tax_office_legal": "7715",
    "timezone": "UTC+3",
    "geo_lat": "55.8783675",
    "geo_lon": "37.6537388",
    "beltway_hit": "IN_MKAD",
    "beltway_distance": null,
    "qc_geo": 0,
    "qc_complete": 0,
    "qc_house": 2,
    "qc": 0,
    "unparsed_parts": null,
    "metro": [
        {
            "distance": 1.1,
            "line": "Калужско-Рижская",
            "name": "Бабушкинская"
        },
        {
            "distance": 1.2,
            "line": "Калужско-Рижская",
            "name": "Медведково"
        },
        {
            "distance": 2.5,
            "line": "Калужско-Рижская",
            "name": "Свиблово"
        }
    ]
}
]
JSON;

    /**
     * Пример ответа для дня рождения
     * @var JSONstring
     */
    public static $exampleBirthday = <<<JSON
[{
	"source": "24/3/12",
	"birthdate": "24.03.2012",
	"qc": 1
}]
JSON;

    /**
     * Пример ответа для расширенного запроса
     * @var JSONstring
     */
    public static $exampleClean = <<<JSON
{
	"structure": [
		"AS_IS",
		"NAME",
		"ADDRESS",
		"PHONE"
	],
	"data": [
		[{
			"source": "1"
		}, {
			"source": "Федотов Алексей",
			"result": "Федотов Алексей",
			"qc": 0
		}, {
			"source": "Москва, Сухонская улица, 11 кв 89",
			"result": "г Москва, ул Сухонская, д 11, кв 89",
			"qc": 0,
			"unparsed_parts": null
		}, {
			"source": "8 916 823 3454",
			"type": "Мобильный",
			"phone": "+7 916 823-34-54",
			"qc": 0
		}],
		[{
			"source": "2"
		}, {
			"source": "Иванов Сергей Владимирович",
			"result": "Иванов Сергей Владимирович",
			"qc": 0
		}, {
			"source": "мск,улица свободы,65,12",
			"result": "г Москва, ул Свободы, д 65, кв 12",
			"qc": 0,
			"unparsed_parts": null
		}, {
			"source": "495 663-12-53",
			"type": "Стационарный",
			"phone": "+7 495 663-12-53",
			"qc": 0
		}],
		[{
			"source": "3"
		}, {
			"source": "Ольга Павловна Ященко",
			"result": "Ященко Ольга Павловна",
			"qc": 0
		}, {
			"source": "Спб, ул Петрозаводская 8",
			"result": "Россия, г Санкт-Петербург, ул Петрозаводская, д 8",
			"qc": 0,
			"unparsed_parts": null
		}, {
			"source": "457 07 25",
			"type": "Стационарный",
			"phone": "+7 812 457-07-25",
			"qc": 1
		}]
	]
}
JSON;

    /**
     * Пример ответа для почты
     * @var JSONstring
     */
    public static $exampleEmail = <<<JSON
[{
	"source": "serega@yandex/ru",
	"email": "serega@yandex.ru",
	"qc": 4
}]
JSON;

    /**
     * Пример ответа для ФИО
     * @var JSONstring
     */
    public static $exampleName = <<<JSON
[{
	"source": "Срегей владимерович иванов",
	"result": "Иванов Сергей Владимирович",
	"result_genitive": "Иванова Сергея Владимировича",
	"result_dative": "Иванову Сергею Владимировичу",
	"result_ablative": "Ивановым Сергеем Владимировичем",
	"surname": "Иванов",
	"name": "Сергей",
	"patronymic": "Владимирович",
	"gender": "М",
	"qc": 1
}]
JSON;

    /**
     * Пример ответа для паспорта
     * @var JSONstring
     */
    public static $examplePassport = <<<JSON
[{
	"source": "4509 235857",
	"series": "45 09",
	"number": "235857",
	"qc": 0
}]
JSON;

    /**
     * Пример ответа для телефона
     * @var JSONstring
     */
    public static $examplePhone = <<<JSON
[{
	"source": "тел 7165219 доб139",
	"type": "Стационарный",
	"phone": "+7 495 716-52-19 доб. 139",
	"country_code": "7",
	"city_code": "495",
	"number": "7165219",
	"extension": "139",
	"provider": "ОАО \"МГТС\"",
	"region": "Москва",
	"timezone": "UTC+3",
	"qc_conflict": 0,
	"qc": 1
}]
JSON;

    /**
     * Пример ответа для машины
     * @var JSONstring
     */
    public static $exampleVehicle = <<<JSON
[{
	"source": "форд фокус",
	"result": "FORD FOCUS",
	"brand": "FORD",
	"model": "FOCUS",
	"qc": 0
}]
JSON;
}